package org.launchcode.launchcart.models;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by LaunchCode
 */
public class TestCustomer {

    @Test
    public void testIsMatchingPassword() {
        String username = "launchcode";
        String password = "learntocode";
        Customer customer = new Customer(username, password);
        assertTrue(customer.isMatchingPassword(password));
    }
}
