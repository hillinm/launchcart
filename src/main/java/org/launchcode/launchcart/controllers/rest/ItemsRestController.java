package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by LaunchCode
 */
@RestController
@RequestMapping(value = "/api/items")
public class ItemsRestController {

    @Autowired
    private ItemRepository itemRepository;

    @GetMapping
    public List<Item> getItems(@RequestParam(required = false) Double price) {

        if (price == null) {
            return itemRepository.findAll();
        }

        return itemRepository.findByPrice(price);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity getSingleItem(@PathVariable("id") int id) {
        Item item = itemRepository.findOne(id);
        if (item == null) {
            return new ResponseEntity("No item found with id " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(item, HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Item createNewItem(@RequestBody Item item) {
        itemRepository.save(item);
        return item;
    }

}
