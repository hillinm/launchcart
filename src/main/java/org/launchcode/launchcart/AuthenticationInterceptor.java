package org.launchcode.launchcart;

import org.launchcode.launchcart.controllers.AbstractCustomerController;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by LaunchCode
 */
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

        if (request.getRequestURI().matches("/api/.*")) {
            return true;
        }

        List<String> nonAuthPages = Arrays.asList("/login", "/register");
        if (!nonAuthPages.contains(request.getRequestURI())) {
            Integer userId = (Integer) request.getSession().getAttribute(AbstractCustomerController.customerSessionKey);
            if (userId != null) {
                User user = customerRepository.findOne(userId);
                if (user != null)
                    return true;
            }
            response.sendRedirect("/login");
            return false;
        }
        return true;
    }

}

